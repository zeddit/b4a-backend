const BACKENDS = [
	"https://zeddit-backend1.herokuapp.com",
	"https://zeddit-backend2.herokuapp.com"
];


Parse.Cloud.define("query", async ({params: {url}}) => {
	for(const backend of BACKENDS) {
		const queryUrl = url
			.replace("https://zeddit.back4app.io", backend)
			.replace("/?", "?");
		try {
			const req = await Parse.Cloud.httpRequest({url: queryUrl});
			return req.text;
		} catch(e) {
			if(e.status === 503) {
				// Heroku error
				continue;
			}
			return e.status;
		}
	}
	return "No reply";
});